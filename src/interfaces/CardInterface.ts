
interface Card {
    id?: string,
    collection_id?: string,
    face_a?: string,
    face_b?: string,
    reversible?: boolean,
    last_viewed?: number,
    revision_frequency?: number,
    last_response?: Boolean | null,
    last_good_response?: number | null,
    last_bad_response?: number | null,
    next_planned_revision?: number | null,
}


export type {
    Card,
}