import { type Card } from '@/interfaces/CardInterface'

interface Collection {
    id?: string,
    name: string,
    description: string,
    creation_timestamp: number,
    color: string,
    card_list: Card[],
}

export type {
    Collection,
}