import { ref, type Ref } from 'vue'
import { defineStore } from 'pinia'
import { v4 } from 'uuid'
import { LocalStorageRepository } from '@/repositories'

import { type Card } from '@/interfaces/CardInterface'
import { type Collection } from '@/interfaces/CollectionInterface'


export const useCollections = defineStore('collections', () => {
  
  const collections = ref<Collection[]>([]);

  async function createCollection(data : Collection) {
    const id = v4();
    const full_data = {
      ...data,
      card_list: [],
      id,
    }

    console.log(collections.value);
    collections.value.push(full_data);
  }


  async function updateCollection(data: any) {
    const collection_index = collections.value
      .findIndex((item) => item.id === data.id)
    collections.value[collection_index] = data
  }


  async function getCollection(id: String) {
    const collection_index = collections.value
      .findIndex((item) => item.id === id)
    const result = collections.value[collection_index]
    return Promise.resolve(result)
  }


  async function createCard(data: Card) {
    const {
      collection_id,
    } = data
    if (!collection_id) {
      throw new Error('collection_id is required')
    }
    const collection = await getCollection(collection_id)
    if (!collection.card_list) {
      collection.card_list = [];
    }
    collection.card_list
        ?.push({
          ...data,
          id: v4(),
        })
  }

  async function updateCard(data: Card) {
    
    console.log(data)
    const {
      collection_id,
      id,
    } = data
    if (!collection_id) {
      throw new Error('collection_id is required')
    }

    if (!id) {
      throw new Error('car id is required')
    }

    const collection = await getCollection(collection_id)
    const card_index = collection.card_list
      ?.findIndex((item) => item.id === id)

    if (card_index === -1) {
      return;
    }
    if (!card_index && card_index !== 0) {
      return;
    }
    if (collection.card_list) {
      collection.card_list[card_index] = {
        ...collection.card_list[card_index],
        ...data,
      }
    }
  }

  async function getCard(id: String, collection_id: String) {
    const collection = await getCollection(collection_id)
    const card_index = collection.card_list
      ?.findIndex((item) => item.id === id)
    if (card_index === -1) {
      return;
    }
    if (!card_index && card_index !== 0) {
      return;
    }
    return collection.card_list?.[card_index]
  }


  async function deleteCard(id: String, collection_id: String) {
    const collection = await getCollection(collection_id)
    const card_index = collection.card_list
      ?.findIndex((item) => item.id === id)

    console.log('card_index', card_index)
    collection.card_list.splice(card_index, 1)
  }


  async function searchCards(collection_id: String) {
    const collection = await getCollection(collection_id)
    return collection.card_list ? collection.card_list : []
  }


  async function saveToLocalStorage() {
    const local_storage_repository = LocalStorageRepository.getInstance()
    await local_storage_repository.setJsonData('collections', collections.value)
  }


  async function loadFromLocalStorage() {
    const local_storage_repository = LocalStorageRepository.getInstance()
    collections.value = await local_storage_repository.getJsonData('collections')
  }

  async function clearLocalStorage() {
    const local_storage_repository = LocalStorageRepository.getInstance()
    await local_storage_repository.clear('collections')
    collections.value = []
  }

  return {
    collections,
    createCollection,
    updateCollection,
    getCollection,
    createCard,
    updateCard,
    getCard,
    deleteCard,
    searchCards,
    saveToLocalStorage,
    loadFromLocalStorage,
    clearLocalStorage,
  }
})
