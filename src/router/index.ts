import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ProcessRevisionView from '@/views/ProcessRevisionView.vue'
import CreateEditCollectionView from '@/views/CreateEditCollectionView.vue'
import CollectionsView from '@/views/CollectionsView.vue'
import AboutView from '@/views/AboutView.vue'
import AdvancedParametersView from '@/views/AdvancedParametersView.vue'
import CreateEditCardView from '@/views/CreateEditCardView.vue'
import CardListView from '@/views/CardListView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView,
    },
    {
      path: '/process-revision',
      name: 'process-revision',
      props: true,
      component: ProcessRevisionView
    },
    {
      path: '/collections',
      name: 'collections',
      component: CollectionsView,
    },
    {
      path: '/collections/create',
      name: 'collections-create',
      component: CreateEditCollectionView,
    },
    {
      path: '/collections/:collection_id/edit',
      name: 'collections-edit',
      props: true,
      component: CreateEditCollectionView,
    },
    {
      path: '/collections/:collection_id/cards',
      name: 'card-list',
      props: true,
      component: CardListView,
    },
    {
      path: '/collections/:collection_id/cards/create',
      name: 'card-create',
      props: true,
      component: CreateEditCardView,
    },
    {
      path: '/collections/:collection_id/cards/:card_id/edit',
      name: 'card-edit',
      props: true,
      component: CreateEditCardView,
    },
    {
      path: '/advanced-params',
      name: 'advanced-params',
      component: AdvancedParametersView,
    },
  ]
})

export default router
