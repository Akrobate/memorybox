import { defineStore } from 'pinia'

interface Card {
  a: String,
  b: String,
}

interface State {
  card_collection_id: null | String,
  card_collection_name: null | String,
  card_list: Card[],
  loading: Boolean,
  error: null | Error,
}

export const usePostStore = defineStore({
  id: 'cards',
  state: (): State => ({
    card_collection_id: null,
    card_collection_name: null,
    card_list: [],
    loading: false,
    error: null,
  }),
  getters: {
    getCardList: (state) => {
      return state.card_list
    }
  }, 
  actions: {
  }
})