
class LocalStorageRepository {

    private constructor() {}

    static instance: LocalStorageRepository | null = null

    private static local_storage_key_prefix: string = "MemoryBox"

    static getInstance() {
        if (LocalStorageRepository.instance === null) {
            LocalStorageRepository.instance = new LocalStorageRepository()
        }
        return LocalStorageRepository.instance
    }

    async getJsonData(key : String): Promise<any> {
        const store_string_data = localStorage
            .getItem(`${LocalStorageRepository.local_storage_key_prefix}_${key}`);
        return JSON.parse(store_string_data || '[]')
    }

    async setJsonData(key : String, json_data: any): Promise<void> {
        const store_string_data = JSON.stringify(json_data)
        localStorage
            .setItem(`${LocalStorageRepository.local_storage_key_prefix}_${key}`, store_string_data)
    }

    async clear(key : String): Promise<any> {
        return localStorage
            .removeItem(`${LocalStorageRepository.local_storage_key_prefix}_${key}`)  
    }

}

export {
    LocalStorageRepository,
}