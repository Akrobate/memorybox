const REVISION_FREQUENCY_REFERENTIAL = [
    {
        name: 'hourly',
        label: 'hourly',
        delay_seconds: 60 * 60,
    },
    {
        name: 'daily',
        label: 'daily',
        delay_seconds: 60 * 60 * 24,
    },
    {
        name: 'every-two-days',
        label: 'every two days',
        delay_seconds: 60 * 60 * 24 * 2,
    },
    {
        name: 'every-three-days',
        label: 'every three days',
        delay_seconds: 60 * 60 * 24 * 3,
    },
    {
        name: 'every-four-days',
        label: 'every four days',
        delay_seconds: 60 * 60 * 24 * 4,
    },
    {
        name: 'every-five-days',
        label: 'every five days',
        delay_seconds: 60 * 60 * 24 * 5,
    },
    {
        name: 'every-six-days',
        label: 'every six days',
        delay_seconds: 60 * 60 * 24 * 6,
    },
    {
        name: 'weekly',
        label: 'weekly',
        delay_seconds: 60 * 60 * 24 * 7,
    },
    {
        name: 'every-two-weeks',
        label: 'every two weeks',
        delay_seconds: 60 * 60 * 24 * 7 * 2,
    },
    {
        name: 'every-three-weeks',
        label: 'every three weeks',
        delay_seconds: 60 * 60 * 24 * 7 * 3,
    },
    {
        name: 'mounthy',
        label: 'mounthy',
        delay_seconds: 60 * 60 * 24 * 7 * 4,
    },
]


export {
    REVISION_FREQUENCY_REFERENTIAL,
}